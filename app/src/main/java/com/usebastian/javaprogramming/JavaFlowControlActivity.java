package com.usebastian.javaprogramming;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class JavaFlowControlActivity extends AppCompatActivity {

    ListView control_flow_listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_flow_control);

        control_flow_listView = findViewById(R.id.java_flow_listView);

        String[] java_flow_array = getResources().getStringArray(R.array.java_control_flow);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_expandable_list_item_1, java_flow_array);

        control_flow_listView.setAdapter(arrayAdapter);

        control_flow_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(JavaFlowControlActivity.this, PDFViewerFlowControlActivity.class);
                intent.putExtra("key_control", position);
                startActivity(intent);

            }
        });

    }
}