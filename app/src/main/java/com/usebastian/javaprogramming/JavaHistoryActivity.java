package com.usebastian.javaprogramming;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class JavaHistoryActivity extends AppCompatActivity {

    ListView historyListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_history);

        historyListView = findViewById(R.id.java_history_listView);

        String[] java_history_array = getResources().getStringArray(R.array.history_list);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_expandable_list_item_1, java_history_array);

        historyListView.setAdapter(arrayAdapter);

        historyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(JavaHistoryActivity.this, PDFHistory.class);
                intent.putExtra("key_history", position);
                startActivity(intent);

            }
        });

    }
}