package com.usebastian.javaprogramming;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class PDFMethods extends AppCompatActivity {

    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_d_f_viewer);

        pdfView = findViewById(R.id.pdfView);

        int java_methods_position = getIntent().getIntExtra("key_methods", 0);

        if (java_methods_position == 0) {

            pdfView.fromAsset("java_methods.pdf").load();
        }
        if (java_methods_position == 1) {

            pdfView.fromAsset("java_parameters.pdf").load();
        }

        if (java_methods_position == 2) {

            pdfView.fromAsset("java_overloading.pdf").load();
        }
        if (java_methods_position == 3){

            pdfView.fromAsset("java_scope.pdf").load();
        }
        if (java_methods_position == 4) {

            pdfView.fromAsset("java_recursion.pdf").load();
        }



    }


}