package com.usebastian.javaprogramming;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class PDFViewerActivity extends AppCompatActivity {

    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_d_f_viewer);

        pdfView = findViewById(R.id.pdfView);

        int java_intro_position = getIntent().getIntExtra("key_position", 0);

        if (java_intro_position == 0) {

            pdfView.fromAsset("java_intro.pdf").load();
        }
        if (java_intro_position == 1) {

            pdfView.fromAsset("java_jdk.pdf").load();
        }

        if (java_intro_position == 2) {

            pdfView.fromAsset("Java_syntax.pdf").load();
        }
        if (java_intro_position == 3) {

            pdfView.fromAsset("java_comments.pdf").load();
        }
        if (java_intro_position == 4) {

            pdfView.fromAsset("Java_variables.pdf").load();
        }

        if (java_intro_position == 5) {

            pdfView.fromAsset("java_data_types.pdf").load();
        }
        if (java_intro_position == 6) {

            pdfView.fromAsset("java_casting.pdf").load();
        }
        if (java_intro_position == 7) {

            pdfView.fromAsset("Java_operators.pdf").load();
        }
        if (java_intro_position == 8) {

            pdfView.fromAsset("java_strings.pdf").load();
        }
        if (java_intro_position == 9) {

            pdfView.fromAsset("java_math.pdf").load();
        }
        if (java_intro_position == 10) {

            pdfView.fromAsset("java_booleans.pdf").load();
        }



    }


}