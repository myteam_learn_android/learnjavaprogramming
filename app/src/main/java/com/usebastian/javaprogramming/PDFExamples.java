package com.usebastian.javaprogramming;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class PDFExamples  extends AppCompatActivity {

    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_d_f_viewer);

        pdfView = findViewById(R.id.pdfView);

        int java_examples = getIntent().getIntExtra("key_examples", 0);

        if (java_examples == 0) {

            pdfView.fromAsset("example2.pdf").load();
        }
        if (java_examples == 1) {

            pdfView.fromAsset("example3.pdf").load();
        }

        if (java_examples == 2) {

            pdfView.fromAsset("example4.pdf").load();
        }

        if (java_examples == 3){

            pdfView.fromAsset("example5.pdf").load();
        }


    }
}
