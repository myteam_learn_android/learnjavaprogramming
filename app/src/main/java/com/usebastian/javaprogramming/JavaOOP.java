package com.usebastian.javaprogramming;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class JavaOOP extends AppCompatActivity {

    ListView javaOPP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_o_o_p);

        javaOPP = findViewById(R.id.java_oop_listView);

        String[] java_methods_array = getResources().getStringArray(R.array.java_oop);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_expandable_list_item_1, java_methods_array);

        javaOPP.setAdapter(arrayAdapter);

        javaOPP.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(JavaOOP.this, PDFOpp.class);
                intent.putExtra("key_oop", position);
                startActivity(intent);

            }
        });
    }
}