package com.usebastian.javaprogramming;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class JavaIntoActivity extends AppCompatActivity {

    ListView javaIntroductionListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_into);


        javaIntroductionListView = findViewById(R.id.java_into_listView);

        String[] java_introduction_array = getResources().getStringArray(R.array.java_introduction_array);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_expandable_list_item_1, java_introduction_array);

        javaIntroductionListView.setAdapter(arrayAdapter);

        javaIntroductionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(JavaIntoActivity.this, PDFViewerActivity.class);
                intent.putExtra("key_position", position);
                startActivity(intent);



            }
        });


    }
}