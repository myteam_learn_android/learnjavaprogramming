package com.usebastian.javaprogramming;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);





        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.logout) {
            Intent logout = new Intent(this, LoginActivity.class);
            startActivity(logout);
        }

        if (item.getItemId() == R.id.nav_home){

            Intent home = new Intent(this, MainActivity.class);
            startActivity(home);
        }




        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void JavaIntoActivity(View view) {

        startActivity(new Intent(this, JavaIntoActivity.class));

    }

    public void JavaControlFlow(View view) {

        startActivity(new Intent(this, JavaFlowControlActivity.class));
    }

    public void JavaMethods(View view) {
        startActivity(new Intent(this, JavaMethods.class));
    }

    public void JavaOPP(View view) {

        startActivity(new Intent(this, JavaOOP.class));
    }

    public void Examples(View view) {

        startActivity(new Intent(this, ExamplesActivity.class));
    }

    public void JavaHistory(View view) {

        startActivity(new Intent(this, JavaHistoryActivity.class));
    }
}