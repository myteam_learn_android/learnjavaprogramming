package com.usebastian.javaprogramming;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ExamplesActivity extends AppCompatActivity {

    ListView examplesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examples);


        examplesListView = findViewById(R.id.java_examples_listView);

        String[] java_examples_array = getResources().getStringArray(R.array.examples);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_expandable_list_item_1, java_examples_array);

        examplesListView.setAdapter(arrayAdapter);

        examplesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(ExamplesActivity.this, PDFExamples.class);
                intent.putExtra("key_examples", position);
                startActivity(intent);

            }
        });

    }
}