package com.usebastian.javaprogramming;

import android.content.Intent;
import android.os.Bundle;
import android.transition.Fade;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreenActivity extends AppCompatActivity {


    private Button createAccount;
    private TextView loginToAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);



        createAccount = findViewById(R.id.create_account_button);
        loginToAccount = findViewById(R.id.login_to_account);

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createAccount = new Intent(SplashScreenActivity.this, RegisterActivity.class);
                startActivity(createAccount);

            }
        });


    }

    public void LoginActivity(View view) {

        startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));

    }




}