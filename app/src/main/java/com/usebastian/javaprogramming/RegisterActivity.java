package com.usebastian.javaprogramming;

import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.vdx.designertoast.DesignerToast;

public class RegisterActivity extends AppCompatActivity {

    //Declaration EditTexts
    EditText editTextUserName, editTextEmail, editTextPassword;

    //Declaration Button
    Button buttonRegister;

    SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        sqLiteHelper = new SQLiteHelper(this);
        initViews();


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    String UserName = editTextUserName.getText().toString();
                    String Email = editTextEmail.getText().toString();
                    String Password = editTextPassword.getText().toString();

                    //Check in the database is there any user associated with  this email
                    if (!sqLiteHelper.isEmailExists(Email)) {

                        //Email does not exist now add new user to database
                        sqLiteHelper.addUser(new User(null, UserName, Email, Password));

                        DesignerToast.Success(RegisterActivity.this, "Success", "Now you are registered!",
                                Gravity.BOTTOM, Toast.LENGTH_LONG, DesignerToast.STYLE_DARK);


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        }, Toast.LENGTH_LONG);
                    } else {

                        //Email exists with email input provided so show error user already exist
                        DesignerToast.Error(RegisterActivity.this, "Error", "Already exists an account with this email!",
                                Gravity.BOTTOM, Toast.LENGTH_LONG, DesignerToast.STYLE_DARK);
                    }


                }
            }
        });
    }

    //this method used to set Login TextView click event


    //this method is used to connect XML views to its Objects
    private void initViews() {
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextUserName = findViewById(R.id.editTextUserName);
        buttonRegister = findViewById(R.id.buttonRegister);

    }

    //This method is used to validate input given by user
    public boolean validate() {

        boolean valid = false;

        //Get values from EditText fields
        String UserName = editTextUserName.getText().toString();
        String Email = editTextEmail.getText().toString();
        String Password = editTextPassword.getText().toString();

        //Handling validation for UserName field
        if (UserName.isEmpty()) {
            valid = false;
            editTextUserName.setError("Please enter valid username!");
        } else {
            if (UserName.length() > 5) {
                valid = true;
                editTextUserName.setError(null);
            } else {
                valid = false;
                editTextUserName.setError("Username is to short!");
            }
        }

        //Handling validation for Email field
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {
            valid = false;
            editTextEmail.setError("Please enter valid email!");
        } else {
            valid = true;
            editTextEmail.setError(null);
        }

        //Handling validation for Password field
        if (Password.isEmpty()) {
            valid = false;
            editTextPassword.setError("Please enter valid password!");
        } else {
            if (Password.length() > 5) {
                valid = true;
                editTextPassword.setError(null);
            } else {
                valid = false;
                editTextPassword.setError("Password is to short!");
            }
        }


        return valid;

    }

}
