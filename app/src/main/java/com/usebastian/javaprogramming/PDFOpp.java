package com.usebastian.javaprogramming;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class PDFOpp extends AppCompatActivity {

    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_d_f_viewer);

        pdfView = findViewById(R.id.pdfView);

        int java_opp = getIntent().getIntExtra("key_oop", 0);

        if (java_opp == 0) {

            pdfView.fromAsset("java_oop.pdf").load();
        }
        if (java_opp == 1) {

            pdfView.fromAsset("java_classes.pdf").load();
        }

        if (java_opp == 2) {

            pdfView.fromAsset("java_attributes.pdf").load();
        }
        if (java_opp == 3) {

            pdfView.fromAsset("java_constructors.pdf").load();
        }
        if (java_opp == 4) {

            pdfView.fromAsset("java_modifiers.pdf").load();
        }

        if (java_opp == 5) {

            pdfView.fromAsset("java_encapsulation.pdf").load();
        }
        if (java_opp == 6) {

            pdfView.fromAsset("java_inheritance.pdf").load();
        }

        if (java_opp == 7) {

            pdfView.fromAsset("java_polymorphism.pdf").load();
        }
        if (java_opp == 8) {

            pdfView.fromAsset("java_interfaces.pdf").load();
        }
        if (java_opp == 9) {

            pdfView.fromAsset("java_array_list.pdf").load();
        }
        if (java_opp == 10) {

            pdfView.fromAsset("java_linked_list.pdf").load();
        }

        if (java_opp == 11) {

            pdfView.fromAsset("java_has_map.pdf").load();
        }
        if (java_opp == 12) {

            pdfView.fromAsset("java_has_set.pdf").load();
        }

        if (java_opp == 13) {

            pdfView.fromAsset("java_wrapper_classes.pdf").load();
        }

        if (java_opp == 14) {

            pdfView.fromAsset("java_exception.pdf").load();
        }



    }


}