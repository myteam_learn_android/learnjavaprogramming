package com.usebastian.javaprogramming;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class PDFViewerFlowControlActivity extends AppCompatActivity {

    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_d_f_viewer);

        pdfView = findViewById(R.id.pdfView);

        int java_control_position = getIntent().getIntExtra("key_control", 0);

        if (java_control_position == 0) {

            pdfView.fromAsset("java_if_else.pdf").load();
        }
        if (java_control_position == 1) {

            pdfView.fromAsset("java_switch.pdf").load();
        }

        if (java_control_position == 2){

            pdfView.fromAsset("java_while_loop.pdf").load();
        }
        if (java_control_position == 3){

            pdfView.fromAsset("java_break_continue.pdf").load();
        }
        if (java_control_position == 4){

            pdfView.fromAsset("java_arrays.pdf").load();
        }

    }


}