package com.usebastian.javaprogramming;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class JavaMethods extends AppCompatActivity {

    ListView javaMethodsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_methods);

        javaMethodsListView = findViewById(R.id.java_methods_listView);

        String[] java_methods_array = getResources().getStringArray(R.array.java_methods);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_expandable_list_item_1, java_methods_array);

        javaMethodsListView.setAdapter(arrayAdapter);

        javaMethodsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(JavaMethods.this, PDFMethods.class);
                intent.putExtra("key_methods", position);
                startActivity(intent);

            }
        });
    }
}